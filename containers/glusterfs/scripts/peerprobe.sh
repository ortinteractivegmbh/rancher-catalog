#!/bin/bash

. /opt/ortinteractive/common.sh

echo "Warte auf den Start aller Service Container"
giddyup service wait scale --timeout=1200

echo "Container starten..."

SELF_NAME=$(curl -s -H 'Accept: application/json' ${META_URL}/self/container| jq -r .name)

IP_METHOD=get_container_primary_ip

peer_wait_hosts()
{
    ready=false
    while [ "$ready" != true ]; do
        echo "Warte auf Daemons..."
        sleep 5
        ready=true
        for peer in $(giddyup service containers -n); do
            IP=$(${IP_METHOD} ${peer})
            timeout $TCP_TIMEOUT bash -c ">/dev/tcp/$IP/$DAEMON_PORT"
            if [ "$?" -ne "0" ]; then
                echo "Peer $peer ist noch nicht bereit"
                ready=false
            fi
        done
    done
}

peer_probe_hosts()
{
    for peer in $(giddyup service containers --exclude-self -n);do
        IP=$(${IP_METHOD} ${peer})
        echo gluster peer probe ${IP}
        gluster peer probe ${IP}
        sleep .5
    done
}

peer_probe()
{
    peer_wait_hosts
    while true; do
        PEER_COUNT=$(gluster pool list|grep -v UUID|wc -l)
        if [ "$(giddyup service scale)" -ne "${PEER_COUNT}" ]; then
            echo "*** unprobed nodes detected"
            peer_probe_hosts
        fi
        sleep 15
    done
}

peer_probe