#!/bin/bash

. /opt/ortinteractive/common.sh

IP_METHOD=get_container_primary_ip


echo "Warte auf den Start aller Container..."
giddyup service wait scale --timeout=1200
echo "Container starten..."

ALLMETA=$(curl -s -H 'Accept: application/json' ${META_URL})
VOLUME_NAME=$(echo ${ALLMETA} | jq -r '.self.service.metadata.volume_name')
ARBITER=$(echo ${ALLMETA} | jq -r '.self.service.metadata.arbiter')
BRICK_PATH="/data/glusterfs/brick1"
VOLUME_PATH="${BRICK_PATH}/${VOLUME_NAME}"
REPLICA_COUNT=$(giddyup service scale)

if [ ! -f ${VOLUME_PATH} ]; then
    mkdir -p "${VOLUME_PATH}"
fi


## Check if this is the Lowest create index
giddyup leader check
if [ "$?" -ne "0" ]; then
    echo "Volume Operationen werden nur vom Leader aufgeführt. Beende Prozess an dieser Stelle."
    sleep 5
    exit 0
fi

echo "Überprüfe alle Peers im Cluster..."
while true; do
    STATE_READY="true"
    for container in $(giddyup service containers -n); do
        IP=$(${IP_METHOD} ${container})
        REPLICA_COUNT=$(giddyup service scale)
        if [ "$(($(gluster --remote-host=${IP} peer status | grep 'Peer in Cluster' | wc -l) + 1))" -ne "${REPLICA_COUNT}" ]; then
            echo "Es fehlen noch Peers im Cluster. Warte..."
            STATE_READY="false"
            break 1
        fi
    done

    if [ "${STATE_READY}" = "true" ]; then
        break 1
    fi
    sleep 5
done
CONTAINER_MNTS=$(giddyup ip stringify --delimiter " " --suffix ":${VOLUME_PATH}" ${STRINGIFY_OPTS})

if [ "$(gluster volume info ${VOLUME_NAME}|grep 'does\ not\ exist'|wc -l)" -ne "1" ]; then
    echo "Creating volume ${VOLUME_NAME}..."

    gluster volume create ${VOLUME_NAME} disperse ${REPLICA_COUNT} redundancy 1 transport tcp ${CONTAINER_MNTS}
   
    sleep 5
fi

if [ "$(gluster volume info ${VOLUME_NAME}| grep ^Status | tr -d '[[:space:]]' | cut -d':' -f2)" = "Created" ]; then
    echo "Standard NFS disable"
    gluster volume set ${VOLUME_NAME} nfs.disable on 
    echo "Starting volume ${VOLUME_NAME}..."
    gluster volume start ${VOLUME_NAME}
fi